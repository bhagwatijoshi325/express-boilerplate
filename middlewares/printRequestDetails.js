const logger = require("../config/logger");

module.exports = async function  printRequestDetails (req, res, next ) {
    logger.info({
        url: req.url,
        method: req.method,
        body: req.body,
        query: req.query,
        params: req.params,
        headers: req.headers
    })
    next();
}