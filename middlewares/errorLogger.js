const logger = require("../config/logger");


const errorLogger = async (err, req, res, next) => {
    console.error("[!!] - ERROR -: ", err);
    // Extract error details
    const errorMsg = err.message || 'Something broke!';
    const errorStack = err.stack || '';
    const errorDetails = {
        message: errorMsg,
        stack: errorStack
    };

    // Extract request details
    const requestDetails = {
        url: req.url,
        method: req.method,
        body: req.body,
        query: req.query,
        params: req.params,
        headers: req.headers
    };

    // Combine error and request details
    const combinedDetails = {
        errorDetails,
        requestDetails
    };

    console.error("[!!] - ERROR -: ", combinedDetails);
    logger.error(combinedDetails);

    res.status(500).send('Something broke!');
}

module.exports = {
    errorLogger
}