// Load environment variables
require('dotenv').config();

const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./config/swagger');
const passport = require('passport');
const mainRouter = require('./routes/index');
const { errorLogger } = require('./middlewares/errorLogger');
const printRequestDetails = require('./middlewares/printRequestDetails');
require('./config/passport');

const setUpServer =  async () => {
    try {
        // establishing and getting mongoDb connection 
        await require("./config/db")();

        // establishing and getting redis connection
        const redisConnection = await require("./config/redis")();

        // initializing express app
        const app = express();

        // parse json and add as js object to req.body
        app.use(express.json());

        // initialize passport as it set up req.login(), req.logout(), req.isAuthenticated(), etc, along with basic state management needed by further strategies to work correctly
        app.use(passport.initialize());
        app.use(printRequestDetails)
        app.use('/', mainRouter);
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
        app.use(errorLogger); // error handler in the last to handle errors
        
        app.listen( process.env.SERVER_PORT , ()=>{
            console.log(`Server is listening at ${process.env.SERVER_PORT}`);
        })

    } catch (err) {
        console.log('error in setting up server : ', err);
    }
}

setUpServer();


