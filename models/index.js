const fs = require("fs");
const path = require("path");

module.exports = (connection) => {
    const models = {};
    const schemaDir = path.join(__dirname , "schemas");
    fs.readdirSync(schemaDir)
        .filter( file => file.endsWith(".js") && file.indexOf(".") != 0)
        .forEach( file => {
            // import each schema file
            const {model , modelName  } = require(path.join(schemaDir , file))(connection);  
            models[modelName] = model;
            // extract model name 
        });

        return models;
    // console.log('connection : ' ,  connection);    
}