const mongoose = require('mongoose');

module.exports = (connection) => {
    const modelName = "User";
    const userSchema = new mongoose.Schema({
        username: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            select: false  // use select('+password') to get
        },
        email: {
            type: String,
            // required: true
        }
    }, {
        timestamps: true
    });

    return {
        model: connection.model(modelName, userSchema),  // actual name will be users. mongoose automatically look for plural and lowercased name.
        modelName
    }
}; 