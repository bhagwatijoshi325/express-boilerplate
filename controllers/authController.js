const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const logger = require('../config/logger');
const { Collection, model } = require('mongoose');
let connection, models;
let UserModel;

( async () => {
    try {
        ({ connection, models } = await require('../config/db')());
        UserModel = models.User;
    } catch (error){
        logger('error in loading dbConnection in the controller : ' , error);
        throw error;
    }
} )();

exports.signup = async (req, res, next) => {
    try {
        let dbCon = await require('../config/db')();
        UserModel = dbCon.models.User;
        const { username, password } = req.body;
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = new UserModel({ username, password: hashedPassword });
        await user.save();
        res.status(201).send('User is Registered');
    } catch (err) {
        next(err, req, res, next);
    }
}


exports.login = async (req, res, next) => {
    try {
        const user = req.user;
        const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET , { expiresIn: process.env.JWT_EXPIRES_IN  });
        res.status(200).json({ token, message: 'success' });
    } catch (err) {
        next(err, req, res, next);
    }
}


exports.secret = async (req, res) => {
    res.status(200).json({ message: 'success' });
}

