const mongoose = require('mongoose');
const initModels = require('../models/index');

let connAndModels = null, initOngoing = false;

const connectDb = async () => {
    try {
        initOngoing = true;
        let connection = await mongoose.createConnection( process.env.MONGODB_URI , {
            useNewUrlParser: true, // ensures string is in compliance with new mongodb updates
            useUnifiedTopology: true, // better connection to DB, in case failure on cluster
        }).asPromise();
        console.log('Successfully connected to mongoDb.');
        const models = initModels(connection);
        // global.models = models;
        connAndModels = { connection , models };
        // console.log('dbConnection : ' , await connection.models.User.find({}));
        console.log('dbConnection : ' , connection.models.User);
        // connect all the models
        initOngoing = false;
        return connection
    } catch (err) {
        initOngoing = false;
        console.log('error in connecting to mongoDb : ' , err);
        throw err;
    }
}

const getModelsAndConn = async  () => {
  if ( !connAndModels ){
    if (!initOngoing){
        await connectDb(); 
        return connAndModels;
    } else{
        return await new Promise((resolve , reject) => {
            let timerId = setInterval(()=>{
                if(connAndModels){
                    clearInterval(timerId);
                    resolve(connAndModels);
                }
            }, 50);
        })
    }
  } else {
    return connAndModels;
  }
} 


module.exports = getModelsAndConn;