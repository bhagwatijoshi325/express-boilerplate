const path = require('path');
const winston = require('winston');

const logger = winston.createLogger({
    defaultMeta: { service: 'common code' },
    transports: [
        new winston.transports.File({
            filename: path.join(__dirname, '..', 'logs', 'error.log'), 
            level: 'error', 
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()
            )
        }),
        new winston.transports.File({
            filename: path.join(__dirname, '..', 'logs', 'combined.log'),
            // only all except error
            level: 'info',
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()
            )
        })
    ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.prettyPrint(),
        )
    }));
}

module.exports = logger;