const swaggerJsdoc = require('swagger-jsdoc');
const path = require('path');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Express API with Swagger',
      version: '1.0.0',
    },
  },
  apis: ['./routes/**/*.js'], // Path to your route files
};

const specs = swaggerJsdoc(options);
module.exports = specs;
