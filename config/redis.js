const { createClient } = require('redis');
let clientAndConnection = null , initOngoing = false;

const connectRedis = async () => {
    try {
        initOngoing = true;
        const client = createClient({
            password: process.env.REDIS_PASSWORD,
            socket: {
                host: process.env.REDIS_HOST,
                port: process.env.REDIS_PORT
            }
        });
        await client.connect();
        console.log('Successfully connected to Redis.');
        clientAndConnection = { client };
        initOngoing = false;
        return client;
    } catch (err) {
        console.log('error in connecting to Redis: ', err);
        initOngoing = false;
        throw err;
    }
};

const getClientAndConnection = async () => {
    if (!clientAndConnection) {
        if (initOngoing){
             return await new Promise((resolve , reject) => {
                setInterval(()=>{
                    if (clientAndConnection) {
                        resolve(clientAndConnection);
                    }
                } , 50)
            })
        }
        await connectRedis();
        return clientAndConnection;
    } else {
        return clientAndConnection;
    }
};

module.exports = getClientAndConnection;
