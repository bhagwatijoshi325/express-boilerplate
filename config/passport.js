const { default: mongoose } = require('mongoose');
const passport = require('passport');
const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const bcrypt = require('bcryptjs');

// Local Strategy
passport.use(  new LocalStrategy( async (username, password, done) => {
    try {
        const UserModel = global.models.User;
        const user = await UserModel.findOne({username}).select("+password");
        if (!user) {
            return done(null , false);
        }
        const isMatched = await bcrypt.compare(password, user.password);
        if (!isMatched) {
            return done(false , null);
        }
        return done(null , user);
    } catch (err) {
        return done(err);
    }
}))


// Jwt Strategy
const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey : process.env.JWT_SECRET
}

passport.use (new JwtStrategy(opts , async (jwtPayload, done) => {   // here in first param you can also give custom name to each strategy.
    try {
        const UserModel = global.models.User;
        const user = await UserModel.findById(jwtPayload.id);
        if (!user) {
            return done(null , false);
        }
        return done(null , user);
    } catch (err) {
        return done(err);
    }
} ))