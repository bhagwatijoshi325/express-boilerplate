const express = require('express');
const authRouter = require('./authRoutes.route');
const router = express.Router();

router.use('/auth', authRouter);

module.exports = router;