const express = require('express');

const router = express.Router();

const passport = require('passport');

const authController = require('../controllers/authController');

/**
 * @swagger
 * /auth/signup:
 *   post:
 *     description: sugnup user
 *     responses:
 *       200:
 *         description: A list of tasks.
 */
router.post('/signup', authController.signup);
router.post('/login', passport.authenticate('local', { session: false }), authController.login); // local is default name
router.get('/secret',
    passport.authenticate('jwt', { session: false }), // jwt is default name
    authController.secret
)

module.exports = router;